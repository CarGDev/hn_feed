import { Component, OnInit } from '@angular/core';
import { News } from '../schemaModel/news.model';
import { ActivatedRoute, Params } from '@angular/router';
import { NewsService } from '../news/news.service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  constructor(private route: ActivatedRoute, private newsService: NewsService) { }

  new: News = {
    _id: ' ',
    created_at: ' ',
    title: ' ',
    url: ' ',
    author: ' ',
    points: ' ',
    story_text: ' ',
    comment_text: ' ',
    num_comments: ' ',
    story_title: ' ',
    parent_id: 25632980,
    story_id: 25632980,
    objectID: '25667958'
  };

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.newsService.getANewById(id)
      .subscribe(news => {
        this.new = news;
      });
    });
  }

}
