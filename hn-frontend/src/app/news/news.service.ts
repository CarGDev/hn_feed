import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News } from '../schemaModel/news.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  routeLink = `http://localhost:3000`;

  getAllNews(): Observable<News[]> {
    return this.http.get<News[]>(`${this.routeLink}/tasks`);
  }

  getANewById(id: string): Observable<News> {
    return this.http.get<News>(`${this.routeLink}/tasks/${id}`);
  }

  delItemById(id: string): any {
    return this.http.delete(`${this.routeLink}/tasks/one/${id}`);
  }
}
