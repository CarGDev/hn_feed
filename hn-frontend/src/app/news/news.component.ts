import { Component, OnInit } from '@angular/core';
import { News } from '../schemaModel/news.model';
import { NewsService } from './news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  constructor(private newsService: NewsService) { }

  news: News[] = [];
  interval: any = null;

  ngOnInit(): void {
    this.fetchNews();
  }

  deleteItem(newId: string, index: number): void {
    this.newsService.delItemById(newId)
    .subscribe(() => {
      this.news.splice(index, 1);
    });
  }

  fetchNews(): void {
    this.newsService.getAllNews()
    .subscribe(news => {
      news.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
      this.news = news;
    });
  }

}
