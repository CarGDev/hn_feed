export interface News {
  _id: string;
  created_at: string;
  title: string;
  url: string;
  author: string;
  points: string;
  story_text: string;
  comment_text: string;
  num_comments: string;
  story_title: string;
  parent_id: number;
  story_id: number;
  objectID: string;
}
