# HN_Feed


<a href="https://gitlab.com/CarGDev/hn_feed/-/blob/master/LICENSE" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>

## Description

[Hacker News](https://gitlab.com/CarGDev/hn_feed) application to read news.

## Installation Docker-compose

First check the current version and update it as show below:

```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
then, configure the permissions:

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

and finally, check the version for docker compose

```bash
docker-compose --version
```

## Project installation

```bash
$ docker-compose build
```

## Running the app

```bash
$ docker-compose up
```

After the project is running open [http://localhost/home](http://localhost:4200/home) in the browser.

![application.gif](./video/Video.gif)


## Check the endpoints

### Postman

- End points use for the communication between backend and frontend - [Endpoints in postman](https://documenter.getpostman.com/view/10727485/TVzPnJk9)

### Swagger

- End points use for the communication between backend and frontend - [Endpoints in swagger](http://localhost:3000/api/#/) **(to check this documentacion all the services from docker compose shall run)**\

## Total project time

This project was tracking by [Wakatime](https://wakatime.com/)

![wakatime.jpg](./video/wakatime.JPG)

## Stay in touch

- Author - [Carlos Gutierrez](https://cargdev.com/)

## License

Project is [MIT licensed](LICENSE).
