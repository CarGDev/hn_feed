import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { DeleteItem } from './interfaces/task';
import { Model } from 'mongoose';
import { CreateTaskDto } from './dto/create-task.dto';

@Injectable()
export class DeleteItemService {
  constructor(
    @InjectModel('DeleteItem') private deleteItemModel: Model<DeleteItem>,
  ) {}

  async getTasks() {
    return await this.deleteItemModel.find();
  }

  async getTask(id: string) {
    const obj = {
      objectID: id,
    };
    return await this.deleteItemModel.findOne(obj);
  }

  async getDelete(id: string) {
    return await this.deleteItemModel.findById(id);
  }

  async upsertTask(task: CreateTaskDto) {
    const checkTask = await this.deleteItemModel.findOne({
      objectID: task.objectID,
    });
    if (checkTask === null) {
      return this.createTask(task);
    }
    return this.updateTask(checkTask._id, task);
  }

  async createTask(task: CreateTaskDto) {
    const newTask = new this.deleteItemModel({ objectID: task });
    return await newTask.save();
  }

  async deleteTask(id: string) {
    const taskDelete = await this.deleteItemModel.findById(id);
    return await this.deleteItemModel.deleteOne(taskDelete);
  }

  async deleteAllTask() {
    return await this.deleteItemModel.remove();
  }

  async updateTask(id: string, task: CreateTaskDto) {
    return await this.deleteItemModel.findOneAndUpdate({ _id: id }, task);
  }
}
