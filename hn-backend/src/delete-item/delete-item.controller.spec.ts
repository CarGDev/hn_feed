import { Test, TestingModule } from '@nestjs/testing';
import { DeleteItemController } from './delete-item.controller';

describe('DeleteItemController', () => {
  let controller: DeleteItemController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DeleteItemController],
    }).compile();

    controller = module.get<DeleteItemController>(DeleteItemController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
