import { Document } from 'mongoose';

export interface DeleteItem extends Document {
  objectID: string;
}
