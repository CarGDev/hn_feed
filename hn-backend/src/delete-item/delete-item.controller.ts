import {
  Controller,
  Get,
  Put,
  Post,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { DeleteItemService } from './delete-item.service';
import { DeleteItem } from './interfaces/task';

@Controller('delete-item')
export class DeleteItemController {
  constructor(private deleteItemService: DeleteItemService) {}

  @Get()
  getTasks(): Promise<DeleteItem[]> {
    return this.deleteItemService.getTasks();
  }

  @Get(':taskId')
  getTask(@Param('taskId') taskId: string) {
    return this.deleteItemService.getTask(taskId);
  }

  @Put(':id')
  updateTask(@Body() task: CreateTaskDto, @Param('id') id: string) {
    return this.deleteItemService.updateTask(id, task);
  }

  @Post()
  createTask(@Body() task: CreateTaskDto) {
    return this.deleteItemService.upsertTask(task);
  }

  @Delete('one/:id')
  deleteTasks(@Param('id') id: string) {
    return this.deleteItemService.deleteTask(id);
  }

  @Delete('all')
  deleteAll() {
    return this.deleteItemService.deleteAllTask();
  }
}
