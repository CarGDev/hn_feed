import { Schema } from 'mongoose';

export const DeleteItemSchema = new Schema({
  objectID: String,
});
