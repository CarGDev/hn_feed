import { Module } from '@nestjs/common';
import { DeleteItemController } from './delete-item.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { DeleteItemService } from './delete-item.service';
import { DeleteItemSchema } from './schemas/delete-item.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'DeleteItem', schema: DeleteItemSchema },
    ]),
  ],
  controllers: [DeleteItemController],
  providers: [DeleteItemService],
  exports: [DeleteItemService],
})
export class DeleteItemModule {}
