import { Test, TestingModule } from '@nestjs/testing';
import { DeleteItemService } from './delete-item.service';

describe('DeleteItemService', () => {
  let service: DeleteItemService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DeleteItemService],
    }).compile();

    service = module.get<DeleteItemService>(DeleteItemService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
