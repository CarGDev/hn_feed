import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { MongooseModule } from '@nestjs/mongoose';
import { InformationModule } from './information/information.module';
import config from './tasks/config/config';

@Module({
  imports: [
    MongooseModule.forRoot(config.mongoURI, {
      useNewUrlParser: true,
      useFindAndModify: false,
    }),
    InformationModule,
    TasksModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
