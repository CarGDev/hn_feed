import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Hack News')
    .setDescription(
      'The Hack News API get the last news from the link: https://hn.algolia.com/api/v1/search_by_date?query=nodejs and save them in a database mounted in mongodb',
    )
    .setVersion('1.0')
    .addTag('nestjs')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.use(cookieParser());
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
