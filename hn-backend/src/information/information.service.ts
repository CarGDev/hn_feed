import { HttpService, Injectable } from '@nestjs/common';
import config from '../tasks/config/config';
import { TasksService } from '../tasks/tasks.service';

@Injectable()
export class InformationService {
  constructor(
    private httpService: HttpService,
    private tasksService: TasksService,
  ) {}

  async findAll() {
    return await this.httpService.get(config.urlHn).toPromise();
  }

  async getFunction() {
    const response = await this.findAll();
    const hits = response.data.hits;
    hits.map((hit) => {
      if (hit.story_title === null) hit.story_title = hit.title;
      if (hit.story_title === null && hit.title === null) return;
      const message = {
        created_at: hit.created_at,
        title: hit.title,
        url: hit.url,
        author: hit.author,
        points: hit.points,
        story_text: hit.story_text,
        comment_text: hit.comment_text,
        num_comments: hit.num_comments,
        story_title: hit.story_title,
        parent_id: hit.parent_id,
        story_id: hit.story_id,
        objectID: hit.objectID,
      };
      this.tasksService.upsertTask(message);
    });
  }
}
