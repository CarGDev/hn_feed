import { Module, HttpModule } from '@nestjs/common';
import { InformationService } from './information.service';
import { InformationController } from './information.controller';
import { TasksModule } from '../tasks/tasks.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    ScheduleModule.forRoot(),
    TasksModule,
  ],
  controllers: [InformationController],
  providers: [InformationService],
})
export class InformationModule {}
