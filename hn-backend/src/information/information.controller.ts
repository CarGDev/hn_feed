import { Controller, Logger, OnModuleInit } from '@nestjs/common';
import { Interval, Cron } from '@nestjs/schedule';
import { InformationService } from './information.service';

@Controller('information')
export class InformationController implements OnModuleInit {
  private readonly logger = new Logger(InformationController.name);

  constructor(private informationService: InformationService) {}

  getInformation() {
    return this.informationService.getFunction();
  }

  @Cron('00 00 * * * *')
  adding() {
    console.log('Cron');
    this.logger.debug(this.getInformation());
  }

  @Interval(3600000)
  addInfo() {
    console.log('Interval');
    this.logger.debug(this.getInformation());
  }

  onModuleInit() {
    console.log('On init');
    this.logger.debug(this.getInformation());
  }
}
