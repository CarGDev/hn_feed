import { Schema } from 'mongoose';

export const TaskSchema = new Schema({
  created_at: String,
  title: String,
  url: String,
  author: String,
  points: String,
  story_text: String,
  comment_text: String,
  num_comments: String,
  story_title: String,
  parent_id: Number,
  story_id: Number,
  objectID: String,
});
