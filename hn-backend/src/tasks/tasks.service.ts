import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task } from './interfaces/task';
import { Model } from 'mongoose';
import { CreateTaskDto } from './dto/create-task.dto';
import { DeleteItemService } from '../delete-item/delete-item.service';

@Injectable()
export class TasksService {
  constructor(
    @InjectModel('Task') private taskModel: Model<Task>,
    private deleteItemService: DeleteItemService,
  ) {}

  async getTasks() {
    return await this.taskModel.find();
  }

  async getTask(id: string) {
    return await this.taskModel.findById(id);
  }

  async upsertTask(task: CreateTaskDto) {
    const taskObject = task.objectID;
    const existDelete = await this.deleteItemService.getTask(taskObject);
    if (existDelete !== null) {
      return 'Not added';
    }
    const checkTask = await this.taskModel.findOne({ objectID: task.objectID });
    if (checkTask === null) {
      return this.createTask(task);
    }
    return this.updateTask(checkTask._id, task);
  }

  async createTask(task: CreateTaskDto) {
    const newTask = new this.taskModel(task);
    return await newTask.save();
  }

  async deleteAll() {
    return await this.taskModel.remove();
  }

  async deleteTask(id: string) {
    const objectIdCheck = await this.taskModel.findById(id);
    const existDelete = await this.deleteItemService.getTask(
      objectIdCheck.objectID,
    );
    if (existDelete !== null) {
      return 'Not added';
    }
    const deletingItem = await this.deleteItemService.upsertTask(
      objectIdCheck.objectID,
    );
    console.log(deletingItem);
    const taskDelete = await this.taskModel.findById(id);
    return await this.taskModel.deleteOne(taskDelete);
  }

  async updateTask(id: string, task: CreateTaskDto) {
    return await this.taskModel.findOneAndUpdate({ _id: id }, task);
  }
}
