import {
  Controller,
  Get,
  Put,
  Post,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TasksService } from './tasks.service';
import { Task } from './interfaces/task';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(): Promise<Task[]> {
    return this.tasksService.getTasks();
  }

  @Get(':taskId')
  getTask(@Param('taskId') taskId: string) {
    return this.tasksService.getTask(taskId);
  }

  @Put(':id')
  updateTask(@Body() task: CreateTaskDto, @Param('id') id: string) {
    return this.tasksService.updateTask(id, task);
  }

  @Post()
  createTask(@Body() task: CreateTaskDto) {
    return this.tasksService.upsertTask(task);
  }

  @Delete('one/:id')
  deleteTasks(@Param('id') id: string) {
    return this.tasksService.deleteTask(id);
  }

  @Delete('all')
  deleteAll() {
    return this.tasksService.deleteAll();
  }
}
